import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './assets/styles/global.scss';
import './assets/styles/minty.bootswatch.css';
import 'font-awesome/css/font-awesome.css';

ReactDOM.render(<App/>, document.getElementById('root'));
