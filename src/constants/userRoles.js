export default Object.freeze({
  administrator: 1,
  referee: 2,
  coach: 3,
  board: 4
});