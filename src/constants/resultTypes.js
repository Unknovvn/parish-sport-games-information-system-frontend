export default Object.freeze({
  teamPoints: 1,
  soloPoints: 101,
  soloTime: 102,
  soloLength: 103
});