import { toast } from 'react-toastify';

export const infoToast = (message) => {
    toast(message, { type: toast.TYPE.INFO })
}

export const successToast = (message) => {
    toast(message, { type: toast.TYPE.SUCCESS })
}

export const warningToast = (message) => {
    toast(message, { type: toast.TYPE.WARNING })
}

export const errorToast = (message) => {
    toast(message, { type: toast.TYPE.ERROR })
}
