import resultTypes from '../../constants/resultTypes';

export const isTeamCompetition = resultTypeId => {
  return resultTypeId < resultTypes.soloPoints;
};