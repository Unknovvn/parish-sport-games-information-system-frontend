export const getResultTypeName = (resultTypeId) => {
  switch (resultTypeId.toString()) {
    case '1':
      return 'Komandinės varžybos: taškai';
    case '101':
      return 'Asmeninės varžybos: taškai';
    case '102':
      return 'Asmeninės varžybos: laikas';
    case '103':
      return 'Asmeninės varžybos: ilgis';
    default:
      return '';
  }
};