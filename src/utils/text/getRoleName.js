export const getRoleName = (roleId) => {
  switch (roleId.toString()) {
    case '1':
      return 'Administratorius';
    case '2':
      return 'Teisėjas';
    case '3':
      return 'Komandos treneris';
    case '4':
      return 'Rezultatų lenta';
    default:
      return '';
  }
};