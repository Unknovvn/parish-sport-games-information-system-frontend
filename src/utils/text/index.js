export { boolToWordConvert } from './boolToWordConvert';
export { getRoleName } from './getRoleName';
export { getResultTypeName } from './getResultTypeName';