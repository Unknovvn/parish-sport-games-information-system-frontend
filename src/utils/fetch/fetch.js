import axios from 'axios';

const API_URL = process.env.REACT_APP_PSG_API_URL;
const PUBLIC_URL = process.env.REACT_APP_PGS_PUBLIC_URL;

export const _fetch = async (
  path,
  {
    body = {},
    method = 'GET',
    secure = false,
  } = {}
) => {
  const token = localStorage.getItem('token');
  const request = {
    method: method,
    validateStatus: status => status < 500,
    headers: {Pragma: 'no-cache', 'Content-Type': 'application/json'},
    params: {},
    data: ''
  };

  if (secure) {
    request.headers = {
      ...request.headers,
      'x-auth-token': `${token}`
    }
  }

  if (method === 'GET') {
    request.params = body;
  } else {
    request.data = JSON.stringify(body);
  }

  try {
    const response = await axios(`${API_URL}/${path}`, request);
    if (response.status === 403) {
      return (window.location.href = `${PUBLIC_URL}/`);
    }

    return response;
  } catch (error) {
    throw new Error(error);
  }
};
