import { _fetch } from '../utils/fetch';
import jwtDecode from 'jwt-decode';
import { warningToast, errorToast } from '../utils/toasts';

const TOKEN_STORAGE_KEY = 'token';
const JWT_HEADER = 'x-auth-token';

export const getCurrentUser = () => {
  try {
    const jwt = localStorage.getItem(TOKEN_STORAGE_KEY);
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
};

export const login = async body => {
  try {
    const { status, headers } = await _fetch('auth/login', { body, method: 'POST' });
    if (status === 200) {
      localStorage.setItem(TOKEN_STORAGE_KEY, headers[JWT_HEADER]);
      return getCurrentUser();
    } else {
      warningToast('Neteisingi prisijungimo duomenys');
      return null;
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
    return null;
  }
};

export const logout = () => {
  localStorage.removeItem(TOKEN_STORAGE_KEY);
};