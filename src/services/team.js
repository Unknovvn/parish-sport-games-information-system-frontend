import { _fetch } from '../utils/fetch';
import { errorToast, warningToast } from '../utils/toasts';

export const getByChampionshipIdAndCoachId = async (championshipId, coachId) => {
  try {
    const { status, data } = await _fetch(`team/${championshipId}/${coachId}`);
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return null;
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const createTeam = async team => {
  try {
    const { status, data } = await _fetch(`team`, { method: 'POST', body: team });
    if (status === 201) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return null;
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const editTeam = async team => {
  try {
    const body = { name: team.name, members: team.members.map(m => ({ id: m.id, name: m.name, surname: m.surname })) };
    const { status, data } = await _fetch(`team/${team.id}`, { method: 'PUT', body });
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return null;
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};