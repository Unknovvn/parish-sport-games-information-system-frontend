import { _fetch } from './../utils/fetch/fetch';
import { errorToast, successToast, warningToast } from '../utils/toasts';

export const getChampionships = async () => {
  try {
    const { status, data } = await _fetch('championship');
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getCoachesChampopnships = async coachId => {
  try {
    const { status, data } = await _fetch(`championship/coach/${coachId}`);
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getActiveChampionships = async () => {
  try {
    const { status, data } = await _fetch(`championship/active`);
    if (status === 200) {
      return data;
    } else {
      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getFinishedChampionships = async () => {
  try {
    const { status, data } = await _fetch(`championship/finished`);
    if (status === 200) {
      return data;
    } else {
      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getChampionshipResults = async championshipId => {
  try {
    const { status, data } = await _fetch(`championship/${championshipId}/results`);
    if (status === 200) {
      return data;
    } else {
      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const addChampionship = async championship => {
  try {
    const { status, data } = await _fetch('championship', { method: 'POST', body: championship });
    if (status === 201) {
      successToast('Čempionatas sėkmingai sukurtas');
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const activateChampionship = async championshipId => {
  try {
    const { status, data } = await _fetch(`championship/activate/${championshipId}`, { method: 'PUT' });
    if (status === 200) {
      successToast(data);
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const finishChampionship = async championshipId => {
  try {
    const { status, data } = await _fetch(`championship/finish/${championshipId}`, { method: 'PUT' });
    if (status === 200) {
      successToast(data);
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const editChampionship = async championship => {
  try {
    const body = { name: championship.name, date: championship.date };
    const { status, data } = await _fetch(`championship/${championship.id}`, { method: 'PUT', body });
    if (status === 200) {
      successToast(data);
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const deleteChampionship = async championshipId => {
  try {
    const { status, data } = await _fetch(`championship/${championshipId}`, { method: 'DELETE' });
    if (status === 200) {
      successToast('Čempionatas sėkmingai išrtrintas');
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};