import { _fetch } from '../utils/fetch';
import { errorToast, successToast, warningToast } from '../utils/toasts';
import UserRoles from '../constants/userRoles';

export const getUsers = async () => {
  try {
    const { status, data } = await _fetch('user');
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getReferees = async () => {
  try {
    const { status, data } = await _fetch(`user/byRole/${UserRoles.referee}`);
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const addUser = async (user) => {
  try {
    const { status, data } = await _fetch('user', { method: 'POST', body: user });
    if (status === 201) {
      successToast('Vartotojas sėkmingai sukurtas');
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const changePassword = async (userId, password, newPassword) => {
  try {
    const { status, data } = await _fetch(
      `user/password/${userId}`,
      { method: 'PUT', body: { password, newPassword } }
    );

    if (status === 200) {
      successToast('Vartotojo slaptažodis sėkmingai pakeistas');
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const deleteUser = async (userId) => {
  try {
    const { status, data } = await _fetch(`user/${userId}`, { method: 'DELETE' });
    if (status === 200) {
      successToast('Vartotojas sėkmingai išrtrintas');
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};