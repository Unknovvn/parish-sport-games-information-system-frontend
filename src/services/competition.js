import { _fetch } from '../utils/fetch';
import { errorToast, successToast, warningToast } from '../utils/toasts';

export const getById = async competitionId => {
  try {
    const { status, data } = await _fetch(`competition/${competitionId}`);
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return null;
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getCompetitions = async championshipId => {
  try {
    const { status, data } = await _fetch(`competition/byChampionship/${championshipId}`);
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getCoachesCompetitions = async (championshipId, coachId) => {
  try {
    const { status, data } = await _fetch(`competition/${championshipId}/coach/${coachId}`);
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getRefereesCompetitions = async (championshipId, refereeId) => {
  try {
    const { status, data } = await _fetch(`competition/${championshipId}/referee/${refereeId}`);
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getSoloCompetitors = async competitionId => {
  try {
    const { status, data } = await _fetch(`competition/${competitionId}/solo-competitors`);
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const getTeamCompetitors = async competitionId => {
  try {
    const { status, data } = await _fetch(`competition/${competitionId}/team-competitors`);
    if (status === 200) {
      return data;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return [];
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const addCompetition = async (competition) => {
  try {
    const { status, data } = await _fetch('competition', { method: 'POST', body: competition });
    if (status === 201) {
      successToast('Varžybos sėkmingai sukurtos');
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const registerTeam = async (competitionId, teamId) => {
  try {
    const body = { competitionId, teamId };
    const { status, data } = await _fetch('competition/registerTeam', { method: 'POST', body });
    if (status === 201) {
      successToast('Komanda sėkmingai registruota');
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const registerTeamMember = async (competitionId, teamId, teamMemberId) => {
  try {
    const body = { competitionId, teamId, teamMemberId };
    const { status, data } = await _fetch('competition/registerTeamMember', { method: 'POST', body });
    if (status === 201) {
      successToast('Komandos narys sėkmingai užregistruotas');
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const activateCompetition = async competitionId => {
  try {
    const { status, data } = await _fetch(`competition/activate/${competitionId}`, { method: 'PUT' });
    if (status === 200) {
      successToast(data);
      return true;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return false;
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const finishSoloCompetition = async (competitionId, results) => {
  try {
    const body = { results };
    const { status, data } = await _fetch(`competition/finish-solo/${competitionId}`, { method: 'PUT', body });
    if (status === 200) {
      successToast(data);
      return true;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return false;
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const finishTeamCompetition = async (competitionId, results) => {
  try {
    const body = { results };
    const { status, data } = await _fetch(`competition/finish-team/${competitionId}`, { method: 'PUT', body });
    if (status === 200) {
      successToast(data);
      return true;
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }

      return false;
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const updateCompetition = async competition => {
  try {
    const body = {
      name: competition.name,
      refereeId: competition.refereeId,
      resultTypeId: competition.resultTypeId
    };

    const { status, data } = await _fetch(`competition/${competition.id}`, { body, method: 'PUT' });
    if (status === 200) {
      successToast(data);
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};

export const deleteCompetition = async (competitionId) => {
  try {
    const { status, data } = await _fetch(`competition/${competitionId}`, { method: 'DELETE' });
    if (status === 200) {
      successToast('Varžybos sėkmingai išrtrintos');
    } else {
      const { error } = data;
      if (error) {
        error.forEach(e => warningToast(e));
      }
    }
  } catch (ex) {
    errorToast('Serveryje įvyko klaida');
  }
};