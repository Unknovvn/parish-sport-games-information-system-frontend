import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './Modal.module.scss';

const Modal = ({show, children, onClose, title}) => {
  const escFunction = useCallback(event => {
    if (event.keyCode === 27) {
      onClose();
    }
  }, [onClose]);

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false);
    return () => document.removeEventListener('keydown', escFunction, false);
  }, [escFunction]);

  if (!show) {
    return null;
  }

  return <div className={styles.backdrop}>
    <div className={styles.modalContainer}>
      <div className={styles.header}>
        <h5>{title}</h5>
        <i className='fa fa-times' onClick={() => onClose()}/>
      </div>
      {children}
    </div>
  </div>
};

Modal.propTypes = {
  show: PropTypes.bool.isRequired,
  children: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string
};

export default Modal;