import React from 'react';
import PropTypes from 'prop-types';

const Button = (
  {
    type = 'button',
    children,
    onClick = () => {
    },
    ...rest
  }) => {

  return <button type={type} className='btn btn-primary' onClick={() => onClick()} {...rest}>
    {children}
  </button>;
};

Button.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  type: PropTypes.string
};

export default Button;