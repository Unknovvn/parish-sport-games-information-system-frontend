import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

const Input = (props) => {
  const {
    name,
    label,
    value,
    error,
    disabled,
    placeholder,
    type = 'text',
    handleChange = () => {
    },
  } = props;

  return (
    <div className={cn('form-group', {'has-danger': error})}>
      <label className="form-control-label" htmlFor={name}>
        {label}
      </label>
      <input
        name={name}
        type={type}
        value={value}
        disabled={disabled}
        placeholder={placeholder}
        onChange={handleChange}
        className={cn('form-control', {'is-invalid': error})}
      />
      {error && <div className="text-danger">{error}</div>}
    </div>
  );
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  type: PropTypes.oneOf(['text', 'password', 'date', 'time', 'number']),
};

export default Input;
