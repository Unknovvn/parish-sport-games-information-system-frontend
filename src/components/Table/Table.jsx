import React from 'react';
import { PropTypes } from 'prop-types';
import Loader from '../Loader';
import styles from './Table.module.scss';

const Table = ({ columns, data, loading, noRecordsLabel }) => {
  if (loading) {
    return (
      <div className='d-flex justify-content-center align-content-center'>
        <Loader style={{ width: 50, height: 50 }}/>
      </div>
    );
  }

  return (
    <table className="table table-hover bg-white">
      <thead>
      <tr>
        {columns.map(column => <th key={`${column.header} ${column.value}`}>{column.header}</th>)}
      </tr>
      </thead>

      <tbody>
      {data.length === 0
        ? <tr>
          <td colSpan={columns.length} className={styles.noDataRow}>{noRecordsLabel}</td>
        </tr>
        : data.map(row =>
          <tr key={row.id}>
            {columns.map(column => <td key={`${row.id} ${column.header} ${column.value}`} className={styles.column}>
              {column.render ? column.render(row) : row[column.value]}
            </td>)}
          </tr>
        )}
      </tbody>
    </table>
  );
};

Table.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  noRecordsLabel: PropTypes.string.isRequired,
  loading: PropTypes.bool
};

export default Table;