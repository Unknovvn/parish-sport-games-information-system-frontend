import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal';
import Button from '../Button';
import OutlineButton from '../OutlineButton';
import styles from './DialogModal.module.scss';

const DeleteUserModal = ({ title, show, onAgree, onClose }) => {
  return <Modal title={title} show={show} onClose={onClose}>
    <div className={styles.content}>
      <Button onClick={() => onAgree()}>Taip</Button>
      <OutlineButton onClick={() => onClose()}>Ne</OutlineButton>
    </div>
  </Modal>;
};

DeleteUserModal.propTypes = {
  title: PropTypes.string.isRequired,
  show: PropTypes.bool.isRequired,
  onAgree: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default DeleteUserModal;