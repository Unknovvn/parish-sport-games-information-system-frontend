import React from 'react';
import PropTypes from 'prop-types';

const Loader = ({ style }) => {
  return (
    <div className="spinner-grow text-secondary" role="status" style={style}>
      <span className="sr-only">Loading...</span>
    </div>
  );
};

Loader.propTypes = {
  style: PropTypes.object
};

export default Loader;