import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import paths from '../../constants/paths';
import AuthContext from '../../store/authContext';

const ProtectedRoute = ({children, ...rest}) => {
  const {user} = useContext(AuthContext);
  if (user) {
    return <Route {...rest}>{children}</Route>;
  }

  return <Redirect to={paths.loginPage}/>;
};

export default ProtectedRoute;
