import React, { useState } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import Button from '../Button';
import styles from './Dropdown.module.scss';

const Dropdown = ({ label, name, value, error, options, onSelect }) => {
  const selectedOption = options.find(o => o.value === value);
  const [selection, setSelection] = useState(null);

  const handleSelect = (value, label) => {
    setSelection(label);
    onSelect(name, value);
  };

  return <div className={styles.wrapper}>
    <div className='dropdown'>
      <Button
        id='dropdownButton'
        className={cx('btn', 'btn-secondary', 'dropdown-toggle', styles.dropdownButton)}
        data-toggle='dropdown'
        aria-haspopup="true"
        aria-expanded="false"
      >
        {selection || (selectedOption && selectedOption.label) || label}
      </Button>
      <div className="dropdown-menu" aria-labelledby="dropdownButton">
        {options.map(option => <Button
          key={option.label}
          className='dropdown-item'
          onClick={() => handleSelect(option.value, option.label)}>
          {option.label}
        </Button>)}
      </div>
    </div>
    {error && <div className="text-danger">{error}</div>}
  </div>;
};

Dropdown.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
  value: PropTypes.number,
  error: PropTypes.string
};

export default Dropdown;