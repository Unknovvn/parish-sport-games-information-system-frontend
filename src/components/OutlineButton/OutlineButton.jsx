import React from 'react';
import PropTypes from 'prop-types';

const OutlineButton = (
  {
    type = 'button',
    children,
    onClick = () => {
    },
    ...rest
  }) => {

  return <button type={type} className='btn btn-outline-primary' onClick={() => onClick()} {...rest}>
    {children}
  </button>;
};

OutlineButton.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  type: PropTypes.string
};

export default OutlineButton;