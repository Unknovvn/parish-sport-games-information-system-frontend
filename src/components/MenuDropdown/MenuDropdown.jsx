import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import styles from './MenuDropdown.module.scss';

const MenuDropdown = ({ options }) => {
  return <span className={cx('dropdown', styles.container)}>
    <i
      id="dropdown-toggler"
      className={cx('fa', 'fa-caret-down', styles.toggler)}
      data-toggle="dropdown"
      aria-haspopup="true"
      aria-expanded="false"
    />
    <div className={cx('dropdown-menu', 'dropdown-menu-right', styles.options)} aria-labelledby="dropdown-toggler">
      {options.map(option =>
        <button
          key={option.label}
          className="dropdown-item"
          type="button"
          onClick={() => option.onClick()}>
          {option.label}
        </button>
      )}
    </div>
  </span>;
};

MenuDropdown.propTypes = {
  toggleIcon: PropTypes.node.isRequired,
  options: PropTypes.array.isRequired
};

export default MenuDropdown;