import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';
import Button from '../Button';
import Input from '../Input';
import Dropdown from '../Dropdown';

const Form = (props) => {
  const { inputFields, validationSchema, submitLabel, onSubmit } = props;
  const [errors, setErrors] = useState({});
  const [data, setData] = useState({});

  useEffect(() => {
    const initialDataState = {};
    inputFields.forEach((inputField) => {
      initialDataState[inputField.name] = inputField.initialValue;
    });

    setData(initialDataState);
  }, [inputFields]);

  const validateForm = () => {
    const { error } = validationSchema.validate(data, { abortEarly: false });
    if (!error) return null;

    const validationErrors = {};
    for (let detail of error.details) {
      validationErrors[detail.path[0]] = detail.message;
    }

    return validationErrors;
  };

  const handleChange = ({ target: input }) => {
    const changedData = { ...data };
    changedData[input.name] = input.value;
    setData(changedData);
  };

  const handleDropdownChange = (name, value) => {
    const changedData = { ...data };
    changedData[name] = value;
    setData(changedData);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const validationErrors = validateForm();
    setErrors(validationErrors || {});
    if (validationErrors) return;
    onSubmit(data);
  };

  return (
    <form onSubmit={handleSubmit}>
      {inputFields.map((inputField) => {
        return inputField.type === 'dropdown'
          ?
          <Dropdown
            key={inputField.name}
            label={inputField.label}
            name={inputField.name}
            value={data[inputField.name] || null}
            error={errors[inputField.name] || ''}
            options={inputField.options}
            onSelect={handleDropdownChange}
          />
          : <Input
            key={inputField.name}
            name={inputField.name}
            label={inputField.label}
            type={inputField.type || 'text'}
            value={data[inputField.name] || ''}
            error={errors[inputField.name] || ''}
            handleChange={handleChange}
          />;
      })}
      <Button type="submit">
        {submitLabel}
      </Button>
    </form>
  );
};

Form.propTypes = {
  inputFields: PropTypes.array.isRequired,
  validationSchema: PropTypes.object.isRequired,
  submitLabel: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default Form;
