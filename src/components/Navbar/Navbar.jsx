import React, { useContext } from 'react';
import { Link, useLocation, useHistory } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import cx from 'classnames';
import paths from '../../constants/paths';
import { logout } from '../../services/auth';
import AuthContext from '../../store/authContext';
import MenuDropdown from '../MenuDropdown';
import styles from './Navbar.module.scss';

const Navbar = (props) => {
  const { links } = props;
  const { user, onLogout } = useContext(AuthContext);
  const { name, surname } = user;
  const { pathname } = useLocation();
  const history = useHistory();

  const signOut = () => {
    logout();
    onLogout();
  };

  const navBarOptions = [
    {
      label: 'Pakeisti slaptažodį',
      onClick: () => history.push(paths.changePassword)
    },
    {
      label: 'Atsijungti',
      onClick: signOut
    }
  ];

  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-primary'>
      <Link className="navbar-brand" to={paths.mainPage}>Parapiada</Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navBarToggler"
        aria-controls="navBarToggler"
        aria-expanded="false"
        aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"/>
      </button>
      <div className="collapse navbar-collapse" id="navBarToggler">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          {links.map(link => (
            <li key={link.label} className={cx('nav-item', { 'active': link.path === pathname })}>
              <Link className='nav-link' to={link.path}>
                {link.label}
              </Link>
            </li>
          ))}
        </ul>
        <div className={styles.userActions}>
          <i className='fa fa-user'/>
          <span className={styles.userName}>{name} {surname}</span>
          <MenuDropdown toggleIcon={<i className='fa fa-caret-down'/>} options={navBarOptions}/>
        </div>
      </div>
    </nav>
  );
};

Navbar.propTypes = {
  links: PropTypes.array.isRequired
};

export default Navbar;