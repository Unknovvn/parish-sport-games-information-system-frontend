import Joi from '@hapi/joi';
import UserRoles from '../../constants/userRoles';

export default Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Vardas yra privalomas laukas',
    'string.empty': 'Vardas privalo turėti reikšmę',
    'string.max': 'Vardas privalo būti trumpesnis nei 100 simbolių'
  }),
  surname: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavardė yra privalomas laukas',
    'string.empty': 'Pavardė privalo turėti reikšmę',
    'string.max': 'Pavardė privalo būti trumpesnis nei 100 simbolių'
  }),
  login: Joi.string().required().empty().max(50).messages({
    'any.required': 'Vartotojo vardas yra privalomas laukas',
    'string.empty': 'Vartotojo vardas privalo turėti reikšmę',
    'string.max': 'Vartotojo vardas privalo būti trumpesnis nei 50 simbolių'
  }),
  password: Joi.string().required().empty().max(50).messages({
    'any.required': 'Slaptažodis yra privalomas laukas',
    'string.empty': 'Slaptažodis privalo turėti reikšmę',
    'string.max': 'Slaptažodis privalo būti trumpesnis nei 50 simbolių'
  }),
  roleId: Joi.number().required().integer().valid(...Object.values(UserRoles)).messages({
    'any.required': 'Rolės id yra privalomas laukas',
    'any.only': 'Neteisinga rolės id reikšmė',
    'number.base': 'Neteisinga rolės id reikšmė',
    'number.integer': 'Neteisinga rolės id reikšmė'
  })
});