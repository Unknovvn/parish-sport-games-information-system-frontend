import Joi from '@hapi/joi';

export default Joi.object({
  password: Joi.string().required().empty().max(50).messages({
    'any.required': 'Slaptažodis yra privalomas laukas',
    'string.empty': 'Slaptažodis privalo turėti reikšmę',
    'string.max': 'Slaptažodis privalo būti trumpesnis nei 50 simbolių'
  }),
  newPassword: Joi.string().required().empty().max(50).messages({
    'any.required': 'Naujas slaptažodis yra privalomas laukas',
    'string.empty': 'Naujas slaptažodis privalo turėti reikšmę',
    'string.max': 'Naujas slaptažodis privalo būti trumpesnis nei 50 simbolių'
  }),
  newPasswordCheck: Joi.string().required().valid(Joi.ref('newPassword')).messages({
    'any.required': 'Pakartotinis slaptažodis yra privalomas laukas',
    'any.only': 'Pakartotinai įvesto slaptažodžio reikšmė turi sutapti su naujo slaptažodžio reikšme',
    'string.empty': 'Pakartotinis slaptažodis privalo turėti reikšmę',
    'string.max': 'Pakartotinis slaptažodis privalo būti trumpesnis nei 50 simbolių'
  })
});