import Joi from '@hapi/joi';

export default Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavadinimas yra privalomas laukas',
    'string.empty': 'Pavadinimas privalo turėti reikšmę',
    'string.max': 'Pavadinimas privalo būti trumpesnis nei 100 simbolių'
  })
});