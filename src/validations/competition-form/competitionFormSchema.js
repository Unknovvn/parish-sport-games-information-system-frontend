import Joi from '@hapi/joi';
import ResultTypes from '../../constants/resultTypes';

export default Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavadinimas yra privalomas laukas',
    'string.empty': 'Pavadinimas privalo turėti reikšmę',
    'string.max': 'Pavadinimas privalo būti trumpesnis nei 100 simbolių'
  }),
  refereeId: Joi.number().required().integer().messages({
    'any.required': 'Teisėjo id yra privalomas laukas',
    'number.base': 'Neteisinga teisėjo id reikšmė',
    'number.integer': 'Neteisinga teisėjo id reikšmė'
  }),
  resultTypeId: Joi.number().required().integer().valid(...Object.values(ResultTypes)).messages({
    'any.required': 'Rezultato tipo id yra privalomas laukas',
    'any.only': 'Neteisinga rezultato tipo id reikšmė',
    'number.base': 'Neteisinga rezultato tipo id reikšmė',
    'number.integer': 'Neteisinga rezultato tipo id reikšmė'
  })
});