import Joi from '@hapi/joi';

export default Joi.object({
    username: Joi.string().empty().max(50).messages({
        'string.empty': 'Prisijungimo vardas privalo turėti reikšmę',
        'string.max': 'Prisijungimo vardas privalo būti trumpesnis nei 50 simbolių'
    }),
    password: Joi.string().empty().max(50).messages({
        'string.empty': 'Slaptažodis privalo turėti reikšmę',
        'string.max': 'Slaptažodis privalo būti trumpesnis nei 50 simbolių'
    })
});