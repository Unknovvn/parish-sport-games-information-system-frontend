const Joi = require('@hapi/joi');

export default Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Vardas yra privalomas laukas',
    'string.empty': 'Vardas privalo turėti reikšmę',
    'string.max': 'Vardas privalo būti trumpesnis nei 100 simbolių'
  }),
  surname: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavardė yra privalomas laukas',
    'string.empty': 'Pavardė privalo turėti reikšmę',
    'string.max': 'Pavardė privalo būti trumpesnis nei 100 simbolių'
  })
});