import Joi from '@hapi/joi';

export default Joi.object({
  teamMemberId: Joi.number().required().integer().messages({
    'any.required': 'Komandos narys yra privalomas',
    'number.base': 'Komandos narys yra privalomas',
    'number.integer': 'Komandos narys yra privalomas'
  })
});