import React, { useEffect, useState } from 'react';
import { getUsers, addUser, deleteUser } from '../../../services/user';
import Button from '../../../components/Button';
import DialogModal from '../../../components/DialogModal';
import UserTable from './UserTable';
import CreateUserModal from './CreateUserModal';
import style from './UserManagement.module.scss';

const UserManagement = () => {
  const [users, setUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showAddUserModal, setShowAddUserModal] = useState(false);
  const [userIdToDelete, setUserIdToDelete] = useState(null);

  const loadUsers = () => {
    getUsers()
      .then(data => {
        setUsers(data);
        setIsLoading(false);
      });
  };

  useEffect(() => {
    loadUsers();
  }, []);

  const handleRemoveUserInit = (userId) => {
    setUserIdToDelete(userId);
    setShowDeleteModal(true);
  };

  const handleRemoveUser = () => {
    deleteUser(userIdToDelete)
      .then(() => {
        const filteredUsers = users.filter(user => user.id !== userIdToDelete);
        setUsers(filteredUsers);
        setUserIdToDelete(null);
        setShowDeleteModal(false);
      });
  };

  const handleUserAddInit = () => {
    setShowAddUserModal(true);
  };

  const handleUserAdd = (formUser) => {
    addUser(formUser)
      .then(() => {
        loadUsers();
        setShowAddUserModal(false);
      });
  };

  return <>
    <DialogModal
      title='Ar tikrai norite ištrinti vartotoją?'
      show={showDeleteModal}
      onAgree={handleRemoveUser}
      onClose={() => setShowDeleteModal(false)}
    />
    <CreateUserModal
      show={showAddUserModal}
      onClose={() => setShowAddUserModal(false)}
      onCreate={handleUserAdd}
    />
    <div className={style.actionButton}>
      <Button onClick={() => handleUserAddInit()}>Sukurti naują vartotoją</Button>
    </div>
    <UserTable
      loading={isLoading}
      data={users}
      onRemoveUser={handleRemoveUserInit}
    />
  </>;
};

export default UserManagement;