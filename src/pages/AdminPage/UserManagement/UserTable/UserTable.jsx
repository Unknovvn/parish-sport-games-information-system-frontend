import React from 'react';
import PropTypes from 'prop-types';
import { getRoleName } from '../../../../utils/text';
import Table from '../../../../components/Table';
import OutlineButton from '../../../../components/OutlineButton';

const UserTable = ({ data, loading, onRemoveUser }) => {
  const tableColumns = [
    {
      header: 'Vardas',
      value: 'name'
    },
    {
      header: 'Pavardė',
      value: 'surname'
    },
    {
      header: 'Rolė',
      render: (row) => <span>{getRoleName(row.roleId)}</span>
    },
    {
      header: '',
      value: 'remove',
      render: (row) => <OutlineButton onClick={() => onRemoveUser(row.id)}>Ištrinti</OutlineButton>
    }
  ];

  return <Table columns={tableColumns} data={data} loading={loading} noRecordsLabel='Nėra sukurtų vartotojų'/>;
};

UserTable.propTypes = {
  data: PropTypes.array.isRequired,
  onRemoveUser: PropTypes.func.isRequired,
  loading: PropTypes.bool
};

export default UserTable;