import React from 'react';
import PropTypes from 'prop-types';
import schema from '../../../../validations/create-user';
import Modal from '../../../../components/Modal';
import Form from '../../../../components/Form';

const CreateUserModal = ({ show, onCreate, onClose }) => {
  const inputFields = [
    {
      name: 'name',
      label: 'Vardas',
      initialValue: ''
    },
    {
      name: 'surname',
      label: 'Pavardė',
      initialValue: ''
    },
    {
      name: 'login',
      label: 'Prisijungimo vardas',
      initialValue: ''
    },
    {
      name: 'password',
      label: 'Slaptažodis',
      initialValue: ''
    },
    {
      name: 'roleId',
      label: 'Rolė',
      type: 'dropdown',
      initialValue: '',
      options: [
        {
          label: 'Administratorius',
          value: 1
        },
        {
          label: 'Teisėjas',
          value: 2
        },
        {
          label: 'Komandos treneris',
          value: 3
        },
        {
          label: 'Rezultatų lenta',
          value: 4
        }
      ]
    }
  ];

  return <Modal show={show} onClose={onClose}>
    <Form
      inputFields={inputFields}
      validationSchema={schema}
      submitLabel='Sukurti'
      onSubmit={onCreate}
    />
  </Modal>;
};

CreateUserModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onCreate: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default CreateUserModal;