import React from 'react';
import PropTypes from 'prop-types';
import schema from '../../../../validations/competition-form';
import ResultTypes from '../../../../constants/resultTypes';
import { getResultTypeName } from '../../../../utils/text';
import Modal from '../../../../components/Modal';
import Form from '../../../../components/Form';

const CompetitionFormModal = ({ competition, referees, show, onCreate, onEdit, onClose }) => {
  const isNewChampionship = Object.keys(competition).length === 0;
  const inputFields = [
    {
      name: 'name',
      label: 'Pavadinimas',
      initialValue: competition.name || ''
    },
    {
      name: 'refereeId',
      label: 'Teisėjas',
      type: 'dropdown',
      initialValue: competition.refereeId || '',
      options: referees.map(referee => ({
        label: `${referee.name} ${referee.surname}`,
        value: referee.id
      }))
    },
    {
      name: 'resultTypeId',
      label: 'Rezultato tipas',
      type: 'dropdown',
      initialValue: competition.resultTypeId || '',
      options: Object.values(ResultTypes).map(resultTypeId => ({
        label: getResultTypeName(resultTypeId),
        value: resultTypeId
      }))
    }
  ];

  return <Modal show={show} onClose={onClose}>
    <Form
      inputFields={inputFields}
      validationSchema={schema}
      submitLabel={isNewChampionship ? 'Sukurti' : 'Redaguoti'}
      onSubmit={isNewChampionship ? onCreate : onEdit}
    />
  </Modal>;
};

CompetitionFormModal.propTypes = {
  competition: PropTypes.object.isRequired,
  referees: PropTypes.array.isRequired,
  show: PropTypes.bool.isRequired,
  onCreate: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default CompetitionFormModal;

