import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { addCompetition, deleteCompetition, getCompetitions, updateCompetition } from '../../../services/competition';
import { getReferees } from '../../../services/user';
import paths from '../../../constants/paths';
import CompetitionTable from './CompetitionTable';
import Button from '../../../components/Button';
import DialogModal from '../../../components/DialogModal/DialogModal';
import CompetitionFormModal from './CompetitionFormModal';
import OutlineButton from '../../../components/OutlineButton';
import styles from '../UserManagement/UserManagement.module.scss';

const CompetitionManagement = () => {
  const history = useHistory();
  const { championshipId } = useParams();
  const [competitions, setCompetitions] = useState([]);
  const [referees, setReferees] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [modalActionCompetitionId, setModalActionCompetitionId] = useState(null);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showCompetitionFormModal, setShowCompetitionFormModal] = useState(false);
  const [formModalCompetition, setFormModalCompetition] = useState({});

  const loadCompetitions = () => {
    getCompetitions(championshipId)
      .then(data => {
        setCompetitions(data);
        setIsLoading(false);
      });
  };

  const loadReferees = () => {
    getReferees()
      .then(data => {
        setReferees(data);
        setIsLoading(false);
      });
  };

  useEffect(() => {
    loadCompetitions();
    loadReferees();
  }, [championshipId]);

  const handleAddCompetitionInit = () => {
    setFormModalCompetition({});
    setShowCompetitionFormModal(true);
  };

  const handleAddCompetition = formCompetition => {
    const model = { championshipId, ...formCompetition };
    addCompetition(model)
      .then(() => {
        loadCompetitions();
        setShowCompetitionFormModal(false);
      });
  };

  const handleEditCompetitionInit = competition => {
    setFormModalCompetition(competition);
    setShowCompetitionFormModal(true);
  };

  const handleEditCompetition = (formCompetition) => {
    const model = { id: formModalCompetition.id, ...formCompetition };
    updateCompetition(model)
      .then(() => {
        const changedCompetitions = [...competitions];
        const changedCompetition = changedCompetitions.find(c => c.id === model.id);
        if (changedCompetition) {
          const index = changedCompetitions.indexOf(changedCompetition);
          changedCompetitions[index] = model;
          setCompetitions(changedCompetitions);
        }

        setFormModalCompetition({});
        setShowCompetitionFormModal(false);
      });
  };

  const handleRemoveCompetitionInit = competitionId => {
    setModalActionCompetitionId(competitionId);
    setShowDeleteModal(true);
  };

  const handleRemoveCompetition = () => {
    deleteCompetition(modalActionCompetitionId)
      .then(() => {
        const filteredUsers = competitions.filter(user => user.id !== modalActionCompetitionId);
        setCompetitions(filteredUsers);
        setModalActionCompetitionId(null);
        setShowDeleteModal(false);
      });
  };

  const handleCompetitionFormClose = () => {
    setFormModalCompetition({});
    setShowCompetitionFormModal(false);
  };

  return <>
    <DialogModal
      title='Ar tikrai norite ištrinti varžybas?'
      show={showDeleteModal}
      onAgree={handleRemoveCompetition}
      onClose={() => setShowDeleteModal(false)}
    />
    <CompetitionFormModal
      competition={formModalCompetition}
      referees={referees}
      show={showCompetitionFormModal}
      onCreate={handleAddCompetition}
      onEdit={handleEditCompetition}
      onClose={handleCompetitionFormClose}
    />
    <div className={styles.actionButton}>
      <Button onClick={() => handleAddCompetitionInit()}>Sukurti naujas varžybas</Button>
    </div>
    <CompetitionTable
      data={competitions}
      referees={referees}
      loading={isLoading}
      onEditChampionship={handleEditCompetitionInit}
      onRemoveChampionship={handleRemoveCompetitionInit}
    />
    <div className={styles.footerDiv}>
      <OutlineButton onClick={() => history.push(paths.championshipManagement)}>
        Grįžti į čempionatų valdymą
      </OutlineButton>
    </div>
  </>;
};

export default CompetitionManagement;