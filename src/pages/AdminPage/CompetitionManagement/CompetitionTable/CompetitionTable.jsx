import React from 'react';
import PropTypes from 'prop-types';
import { getResultTypeName } from '../../../../utils/text';
import Table from '../../../../components/Table';
import OutlineButton from '../../../../components/OutlineButton';

const CompetitionTable = (
  {
    data,
    referees,
    loading,
    onEditChampionship,
    onRemoveChampionship
  }) => {
  const tableColumns = [
    {
      header: 'Pavadinimas',
      value: 'name'
    },
    {
      header: 'Teisėjas',
      render: (row) => {
        const referee = referees.find(r => r.id === row.refereeId);
        if (referee) {
          return `${referee.name} ${referee.surname}`;
        }

        return '';
      }
    },
    {
      header: 'Rezultato tipas',
      render: (row) => getResultTypeName(row.resultTypeId)
    },
    {
      header: '',
      value: 'edit',
      render: (row) => (!row.active && !row.finished) &&
        <OutlineButton onClick={() => onEditChampionship(row)}>Redaguoti</OutlineButton>
    },
    {
      header: '',
      value: 'remove',
      render: (row) => !row.active &&
        <OutlineButton onClick={() => onRemoveChampionship(row.id)}>Ištrinti</OutlineButton>
    }
  ];

  return <Table columns={tableColumns} data={data} loading={loading} noRecordsLabel='Nėra sukurtų varžybų'/>;
};

CompetitionTable.propTypes = {
  data: PropTypes.array.isRequired,
  referees: PropTypes.array.isRequired,
  onEditChampionship: PropTypes.func.isRequired,
  onRemoveChampionship: PropTypes.func.isRequired,
  loading: PropTypes.bool
};

export default CompetitionTable;