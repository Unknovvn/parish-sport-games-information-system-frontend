import React from 'react';
import PropTypes from 'prop-types';
import { toInputDateFormat } from '../../../../utils/dates';
import schema from '../../../../validations/championship-form';
import Modal from '../../../../components/Modal';
import Form from '../../../../components/Form';

const ChampionshipFormModal = ({ championship, show, onCreate, onEdit, onClose }) => {
  const isNewChampionship = Object.keys(championship).length === 0;
  const inputFields = [
    {
      name: 'name',
      label: 'Pavadinimas',
      initialValue: championship.name || ''
    },
    {
      name: 'date',
      label: 'Data',
      initialValue: toInputDateFormat(championship.date) || '',
      type: 'date'
    }
  ];

  return <Modal show={show} onClose={onClose}>
    <Form
      inputFields={inputFields}
      validationSchema={schema}
      submitLabel={isNewChampionship ? 'Sukurti' : 'Redaguoti'}
      onSubmit={isNewChampionship ? onCreate : onEdit}
    />
  </Modal>;
};

ChampionshipFormModal.propTypes = {
  championship: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  onCreate: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default ChampionshipFormModal;

