import React from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import paths from '../../../../constants/paths';
import { boolToWordConvert } from '../../../../utils/text';
import { toInputDateFormat } from '../../../../utils/dates';
import Table from '../../../../components/Table';
import OutlineButton from '../../../../components/OutlineButton';

const ChampionshipTable = (
  {
    data,
    loading,
    onActivateChampionship,
    onFinishChampionship,
    onEditChampionship,
    onRemoveChampionship
  }) => {
  const history = useHistory();

  const navigateToCompetitionManagement = championshipId => {
    const path = paths.competitionManagement.replace(':championshipId', championshipId);
    history.push(path);
  };

  const navigateToHistoryResults = championshipId => {
    const path = paths.historyResults.replace(':championshipId', championshipId);
    history.push(path);
  };

  const tableColumns = [
    {
      header: 'Pavadinimas',
      value: 'name'
    },
    {
      header: 'Data',
      render: row => <span>{toInputDateFormat(row.date)}</span>
    },
    {
      header: 'Aktyvus',
      render: row => <span>{boolToWordConvert(row.active)}</span>
    },
    {
      header: 'Užbaigtas',
      render: row => <span>{boolToWordConvert(row.finished)}</span>
    },
    {
      header: '',
      value: 'competition-management',
      render: row => !row.active && !row.finished &&
        <OutlineButton onClick={() => navigateToCompetitionManagement(row.id)}>
          Varžybų valdymas
        </OutlineButton>
    },
    {
      header: '',
      value: 'activate',
      render: row => !row.active && !row.finished &&
        <OutlineButton onClick={() => onActivateChampionship(row.id)}>Aktyvuoti</OutlineButton>
    },
    {
      header: '',
      value: 'finish',
      render: row => row.active && !row.finished &&
        <OutlineButton onClick={() => onFinishChampionship(row.id)}>Užbaigti</OutlineButton>
    },
    {
      header: '',
      value: 'edit',
      render: row => !row.active && !row.finished &&
        <OutlineButton onClick={() => onEditChampionship(row)}>Redaguoti</OutlineButton>
    },
    {
      header: '',
      value: 'remove',
      render: row => !row.active && !row.finished &&
        <OutlineButton onClick={() => onRemoveChampionship(row.id)}>Ištrinti</OutlineButton>
    },
    {
      header: '',
      value: 'history-results',
      render: row => row.finished &&
        <OutlineButton onClick={() => navigateToHistoryResults(row.id)}>Peržiūrėti rezultatus</OutlineButton>
    }
  ];

  return <Table columns={tableColumns} data={data} loading={loading} noRecordsLabel='Nėra sukurtų čempionatų'/>;
};

ChampionshipTable.propTypes = {
  data: PropTypes.array.isRequired,
  onActivateChampionship: PropTypes.func.isRequired,
  onFinishChampionship: PropTypes.func.isRequired,
  onEditChampionship: PropTypes.func.isRequired,
  onRemoveChampionship: PropTypes.func.isRequired,
  loading: PropTypes.bool
};

export default ChampionshipTable;
