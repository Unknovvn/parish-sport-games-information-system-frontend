import React, { useEffect, useState } from 'react';
import {
  getChampionships,
  addChampionship,
  activateChampionship,
  finishChampionship,
  editChampionship,
  deleteChampionship
} from '../../../services/championship';
import Button from '../../../components/Button';
import DialogModal from '../../../components/DialogModal';
import ChampionshipTable from './ChampionshipTable';
import ChampionshipFormModal from './ChampionshipFormModal';
import style from '../UserManagement/UserManagement.module.scss';

const ChampionshipManagement = () => {
  const [championships, setChampionships] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [modalActionChampId, setModalActionChampId] = useState(null);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showActivateModal, setShowActivateModal] = useState(false);
  const [showFinishModal, setShowFinishModal] = useState(false);
  const [showChampFormModal, setShowChampFormModal] = useState(false);
  const [formModalChamp, setFormModalChamp] = useState({});

  const loadChampionships = () => {
    getChampionships()
      .then(data => {
        setChampionships(data);
        setIsLoading(false);
      });
  };

  useEffect(() => {
    loadChampionships();
  }, []);

  const handleRemoveChampInit = (champId) => {
    setModalActionChampId(champId);
    setShowDeleteModal(true);
  };

  const handleRemoveChamp = () => {
    deleteChampionship(modalActionChampId)
      .then(() => {
        const filteredUsers = championships.filter(user => user.id !== modalActionChampId);
        setChampionships(filteredUsers);
        setModalActionChampId(null);
        setShowDeleteModal(false);
      });
  };

  const handleActivateChampInit = (champId) => {
    setModalActionChampId(champId);
    setShowActivateModal(true);
  };

  const handleActivateChamp = () => {
    activateChampionship(modalActionChampId)
      .then(() => {
        loadChampionships();
        setModalActionChampId(null);
        setShowActivateModal(false);
      });
  };

  const handleFinishChampInit = (champId) => {
    setModalActionChampId(champId);
    setShowFinishModal(true);
  };

  const handleFinishChamp = () => {
    finishChampionship(modalActionChampId)
      .then(() => {
        loadChampionships();
        setModalActionChampId(null);
        setShowFinishModal(false);
      });
  };

  const handleChampAddInit = () => {
    setFormModalChamp({});
    setShowChampFormModal(true);
  };

  const handleChampAdd = formChamp => {
    addChampionship(formChamp)
      .then(() => {
        loadChampionships();
        setShowChampFormModal(false);
      });
  };

  const handleChampEditInit = championship => {
    setFormModalChamp(championship);
    setShowChampFormModal(true);
  };

  const handleChampEdit = formChamp => {
    const model = { id: formModalChamp.id, ...formChamp };
    editChampionship(model)
      .then(() => {
        const changedChampionships = [...championships];
        const changedChampionship = changedChampionships.find(c => c.id === model.id);
        if (changedChampionship) {
          const index = changedChampionships.indexOf(changedChampionship);
          changedChampionships[index] = model;
          setChampionships(changedChampionships);
        }

        setFormModalChamp({});
        setShowChampFormModal(false);
      });
  };

  const handleChampFormClose = () => {
    setFormModalChamp({});
    setShowChampFormModal(false);
  };

  return <>
    <DialogModal
      title='Ar tikrai norite ištrinti čempionatą?'
      show={showDeleteModal}
      onAgree={handleRemoveChamp}
      onClose={() => setShowDeleteModal(false)}
    />
    <DialogModal
      title='Ar tikrai norite aktyvuoti čempionatą?'
      show={showActivateModal}
      onAgree={handleActivateChamp}
      onClose={() => setShowActivateModal(false)}
    />
    <DialogModal
      title='Ar tikrai norite užbaigti čempionatą?'
      show={showFinishModal}
      onAgree={handleFinishChamp}
      onClose={() => setShowFinishModal(false)}
    />
    <ChampionshipFormModal
      championship={formModalChamp}
      show={showChampFormModal}
      onCreate={handleChampAdd}
      onEdit={handleChampEdit}
      onClose={handleChampFormClose}
    />
    <div className={style.actionButton}>
      <Button onClick={() => handleChampAddInit()}>Sukurti naują čempionatą</Button>
    </div>
    <ChampionshipTable
      loading={isLoading}
      data={championships}
      onActivateChampionship={handleActivateChampInit}
      onFinishChampionship={handleFinishChampInit}
      onEditChampionship={handleChampEditInit}
      onRemoveChampionship={handleRemoveChampInit}
    />
  </>;
};

export default ChampionshipManagement;