import React from 'react';
import { Redirect, Switch } from 'react-router-dom';
import paths from '../../constants/paths';
import Navbar from '../../components/Navbar';
import Container from '../../components/Container';
import ProtectedRoute from '../../components/ProtectedRoute';
import UserManagement from './UserManagement';
import ChampionshipManagement from './ChampionshipManagement';
import ChangePassword from '../HomePage/ChangePassword';
import CompetitionManagement from './CompetitionManagement';
import HistoryResultsTable from '../HistoryResultsTable';

const AdminPage = () => {
  const navbarLinks = [
    {
      label: 'Vartotojų valdymas',
      path: paths.userManagement
    },
    {
      label: 'Čempionatų valdymas',
      path: paths.championshipManagement
    }
  ];

  return (
    <>
      <Navbar links={navbarLinks}/>
      <Container>
        <Switch>
          <ProtectedRoute path={paths.userManagement} exact component={UserManagement}/>
          <ProtectedRoute path={paths.championshipManagement} exact component={ChampionshipManagement}/>
          <ProtectedRoute path={paths.competitionManagement} exact component={CompetitionManagement}/>
          <ProtectedRoute path={paths.historyResults} exact component={HistoryResultsTable}/>
          <ProtectedRoute path={paths.changePassword} exact component={ChangePassword}/>
          <Redirect to={paths.userManagement}/>
        </Switch>
      </Container>
    </>
  );
};

export default AdminPage;
