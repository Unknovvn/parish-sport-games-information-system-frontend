import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getChampionshipResults } from '../../services/championship';
import Table from '../../components/Table';
import styles from './HistoryResultsTable.module.scss';

const HistoryResultsTable = () => {
  const { championshipId } = useParams();
  const [loading, setLoading] = useState(true);
  const [results, setResults] = useState([]);

  const loadResults = () => {
    getChampionshipResults(championshipId)
      .then(data => {
        setResults(data);
        setLoading(false);
      });
  };

  useEffect(() => {
    loadResults();
  }, [championshipId]);

  const tableColumns = [
    {
      header: 'Komandos pavadinimas',
      value: 'teamName'
    },
    {
      header: 'Surinkti taškai',
      value: 'points'
    }
  ];

  return <div className={styles.tableContainer}>
    <Table
      columns={tableColumns}
      data={results}
      loading={loading}
      noRecordsLabel='Čempionatas neturi užbaigtų varžybų'
    />
  </div>;
};

export default HistoryResultsTable;