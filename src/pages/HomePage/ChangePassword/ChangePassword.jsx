import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import paths from '../../../constants/paths';
import AuthContext from '../../../store/authContext';
import { changePassword } from '../../../services/user';
import schema from '../../../validations/change-user-password';
import Form from '../../../components/Form';
import styles from './ChangePassword.module.scss';

const ChangePassword = () => {
  const history = useHistory();
  const { user } = useContext(AuthContext);

  const handleFormSubmit = async (data) => {
    changePassword(user.id, data.password, data.newPassword)
      .then(() => {
        history.push(paths.mainPage);
      });
  };

  const inputFields = [
    {
      name: 'password',
      label: 'Dabartinis slaptažodis',
      initialValue: '',
      type: 'password'
    },
    {
      name: 'newPassword',
      label: 'Naujas slaptažodis',
      initialValue: ''
    },
    {
      name: 'newPasswordCheck',
      label: 'Įveskite slaptažodį pakartotinai',
      initialValue: ''
    }
  ];

  return <div className={styles.container}>
    <h2>Slaptažodžio keitimas</h2>
    <Form
      inputFields={inputFields}
      validationSchema={schema}
      submitLabel="Pakeisti"
      onSubmit={handleFormSubmit}
    />
  </div>;
};

export default ChangePassword;