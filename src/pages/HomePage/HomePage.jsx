import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';
import AuthContext from '../../store/authContext';
import userRoles from '../../constants/userRoles';
import paths from '../../constants/paths';
import AdminPage from './../AdminPage/AdminPage';
import CoachPage from './../CoachPage/CoachPage';
import RefereePage from './../RefereePage/RefereePage';
import BoardPage from './../BoardPage/BoardPage';

const HomePage = () => {
  const {user} = useContext(AuthContext);
  switch (user.role_id) {
    case userRoles.administrator:
      return <AdminPage/>;
    case userRoles.coach:
      return <CoachPage/>;
    case userRoles.referee:
      return <RefereePage/>;
    case userRoles.board:
      return <BoardPage/>;
    default:
      return <Redirect to={paths.loginPage}/>;
  }
};

export default HomePage;
