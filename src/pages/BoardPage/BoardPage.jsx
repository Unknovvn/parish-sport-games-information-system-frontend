import React from 'react';
import { Redirect, Switch } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import paths from '../../constants/paths';
import ProtectedRoute from '../../components/ProtectedRoute';
import Container from '../../components/Container';
import BoardChampionshipTable from './BoardChampionshipTable';
import ChampionshipResultsTable from './ChampionshipResultsTable';

const BoardPage = () => {
  const navbarLinks = [
    {
      label: 'Čempionatų lentelė',
      path: paths.boardChampionships
    }
  ];

  return <>
    <Navbar links={navbarLinks}/>
    <Container>
      <Switch>
        <ProtectedRoute exact path={paths.boardChampionships} component={BoardChampionshipTable}/>
        <ProtectedRoute exact path={paths.boardChampionshipResults} component={ChampionshipResultsTable}/>
        <Redirect to={paths.boardChampionships}/>
      </Switch>
    </Container>
  </>;
};

export default BoardPage;
