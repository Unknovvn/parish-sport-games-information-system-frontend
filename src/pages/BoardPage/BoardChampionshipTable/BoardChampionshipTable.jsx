import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import paths from '../../../constants/paths';
import { getActiveChampionships } from '../../../services/championship';
import { toInputDateFormat } from '../../../utils/dates';
import Table from '../../../components/Table';
import OutlineButton from '../../../components/OutlineButton';
import styles from './BoardChampionshipTable.module.scss';

const BoardChampionshipTable = () => {
  const history = useHistory();
  const [championships, setChampionships] = useState([]);
  const [loading, setLoading] = useState(false);

  const loadBoardChampionships = () => {
    getActiveChampionships()
      .then(data => {
        setChampionships(data);
        setLoading(false);
      });
  };

  const navigateToWatchResults = championshipId => {
    const path = paths.boardChampionshipResults.replace(':championshipId', championshipId);
    history.push(path);
  };

  useEffect(() => {
    loadBoardChampionships();
  }, []);

  const tableColumns = [
    {
      header: 'Pavadinimas',
      value: 'name'
    },
    {
      header: 'Data',
      render: row => <span>{toInputDateFormat(row.date)}</span>
    },
    {
      header: '',
      value: 'watch-results',
      render: row => <OutlineButton onClick={() => navigateToWatchResults(row.id)}>
        Žiūrėti rezultatus
      </OutlineButton>
    }
  ];

  return <>
    <div className={styles.tableContainer}>
      <Table
        columns={tableColumns}
        data={championships}
        loading={loading}
        noRecordsLabel='Nėra aktyvių čempionatų'
      />
    </div>
  </>;
};

export default BoardChampionshipTable;