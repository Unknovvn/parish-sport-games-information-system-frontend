import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import socketIOClient from 'socket.io-client';
import serverPaths from '../../../constants/server-paths';
import { getChampionshipResults } from '../../../services/championship';
import Table from '../../../components/Table';
import styles from './ChampionshipResultsTable.module.scss';

const ChampionshipResultsTable = () => {
  const { championshipId } = useParams();
  const [loading, setLoading] = useState(true);
  const [results, setResults] = useState([]);

  const loadResults = () => {
    getChampionshipResults(championshipId)
      .then(data => {
        setResults(data);
        setLoading(false);
      });
  };

  useEffect(() => {
    loadResults();
    const socket = socketIOClient(serverPaths.backendPath);
    socket.on('competitionFinished', () => {
      loadResults();
    });
  }, [championshipId]);

  const tableColumns = [
    {
      header: 'Komandos pavadinimas',
      value: 'teamName'
    },
    {
      header: 'Surinkti taškai',
      value: 'points'
    }
  ];

  return <div className={styles.tableContainer}>
    <Table
      columns={tableColumns}
      data={results}
      loading={loading}
      noRecordsLabel='Čempionatas neturi užbaigtų varžybų'
    />
  </div>;
};

export default ChampionshipResultsTable;