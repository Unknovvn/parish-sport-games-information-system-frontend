import React from 'react';
import { Redirect, Switch } from 'react-router-dom';
import paths from '../../constants/paths';
import Navbar from '../../components/Navbar';
import ProtectedRoute from '../../components/ProtectedRoute';
import Container from '../../components/Container';
import ChangePassword from '../HomePage/ChangePassword';
import CoachChampionshipTable from './CoachChampionshipTable';
import CoachCompetitionTable from './CoachCompetitionTable';
import TeamMembersTable from './TeamMembersTable';
import HistoryResultsTable from '../HistoryResultsTable';
import HistoryChampionshipsTable from '../HistoryChampionshipsTable';

const CoachPage = () => {
  const navbarLinks = [
    {
      label: 'Dalyvavimas čempionatuose',
      path: paths.coachChampionships
    },
    {
      label: 'Istoriniai čempionatų duomenys',
      path: paths.historyChampionships
    }
  ];

  return <>
    <Navbar links={navbarLinks}/>
    <Container>
      <Switch>
        <ProtectedRoute exact path={paths.changePassword} component={ChangePassword}/>
        <ProtectedRoute exact path={paths.coachChampionships} component={CoachChampionshipTable}/>
        <ProtectedRoute exact path={paths.coachCompetitions} component={CoachCompetitionTable}/>
        <ProtectedRoute exact path={paths.createTeam} component={TeamMembersTable}/>
        <ProtectedRoute exact path={paths.editTeam} component={TeamMembersTable}/>
        <ProtectedRoute exact path={paths.historyResults} component={HistoryResultsTable}/>
        <ProtectedRoute exact path={paths.historyChampionships} component={HistoryChampionshipsTable}/>
        <Redirect to={paths.coachChampionships}/>
      </Switch>
    </Container>
  </>;
};

export default CoachPage;
