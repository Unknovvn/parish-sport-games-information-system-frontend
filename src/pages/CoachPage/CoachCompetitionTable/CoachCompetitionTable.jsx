import React, { useContext, useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import AuthContext from '../../../store/authContext';
import paths from '../../../constants/paths';
import { getCoachesCompetitions, registerTeam, registerTeamMember } from '../../../services/competition';
import { getByChampionshipIdAndCoachId } from '../../../services/team';
import { isTeamCompetition } from '../../../utils/resultType';
import OutlineButton from '../../../components/OutlineButton';
import Table from '../../../components/Table';
import RegisterTeamMemberFormModal from '../RegisterTeamMemberFormModal';
import styles from './CoachCompetitionTable.module.scss';

const CoachCompetitionTable = () => {
  const history = useHistory();
  const { championshipId } = useParams();
  const { user } = useContext(AuthContext);
  const [loading, setLoading] = useState(true);
  const [team, setTeam] = useState({});
  const [competitions, setCompetitions] = useState([]);
  const [showRegisterTeamMemberModal, setShowRegisterTeamMemberModal] = useState(false);
  const [competitionIdToRegisterMember, setCompetitionIdToRegisterMember] = useState(null);

  const loadTeam = () => {
    getByChampionshipIdAndCoachId(championshipId, user.id)
      .then(data => {
        if (data) {
          setTeam(data);
        }
      });
  };

  const loadCoachCompetitions = () => {
    getCoachesCompetitions(championshipId, user.id)
      .then(data => {
        setCompetitions(data);
        setLoading(false);
      });
  };

  useEffect(() => {
    loadTeam();
    loadCoachCompetitions();
  }, [championshipId, user.id]);

  const onRegisterTeam = competition => {
    registerTeam(competition.id, team.id)
      .then(() => {
        loadCoachCompetitions();
      });
  };

  const onRegisterMemberInit = competitionId => {
    setCompetitionIdToRegisterMember(competitionId);
    setShowRegisterTeamMemberModal(true);
  };

  const onRegisterMember = (formModel) => {
    registerTeamMember(competitionIdToRegisterMember, team.id, formModel.teamMemberId)
      .then(() => {
        setCompetitionIdToRegisterMember(null);
        setShowRegisterTeamMemberModal(false);
        loadCoachCompetitions();
      });
  };

  const onCloseRegisterMemberModal = () => {
    setShowRegisterTeamMemberModal(false);
    setCompetitionIdToRegisterMember(null);
  };

  const tableColumns = [
    {
      header: 'Pavadinimas',
      value: 'name'
    },
    {
      header: 'Būsena',
      value: 'registracija',
      render: row => row.finished
        ? 'Varžybos užbaigtos'
        : row.isRegistered
          ? 'Registracija įvykdyta'
          : 'Galima registracija'
    },
    {
      header: '',
      value: 'register-team',
      render: row => (!row.isRegistered && !row.active && !row.finished) &&
        <OutlineButton onClick={() => {
          isTeamCompetition(row.resultTypeId) ? onRegisterTeam(row) : onRegisterMemberInit(row.id);
        }}>
          Registruotis
        </OutlineButton>
    }
  ];

  return <>
    <RegisterTeamMemberFormModal
      show={showRegisterTeamMemberModal}
      onSubmit={onRegisterMember}
      teamMembers={team.members || []}
      onClose={() => onCloseRegisterMemberModal()}
    />
    <div className={styles.tableContainer}>
      <Table
        columns={tableColumns}
        data={competitions}
        loading={loading}
        noRecordsLabel='Čempionatas neturi sukurtų varžybų'
      />
    </div>
    <div className={styles.footerDiv}>
      <OutlineButton onClick={() => history.push(paths.coachChampionships)}>
        Grįžti į čempionatų sąrašą
      </OutlineButton>
    </div>
  </>;
};

export default CoachCompetitionTable;