import React from 'react';
import schema from '../../../validations/register-team-member';
import Modal from '../../../components/Modal';
import Form from '../../../components/Form';
import PropTypes from 'prop-types';

const RegisterTeamMemberFormModal = ({ show, teamMembers, onSubmit, onClose }) => {
  const inputFields = [
    {
      name: 'teamMemberId',
      label: 'Komandos narys',
      type: 'dropdown',
      initialValue: '',
      options: teamMembers.map(tm => ({
        label: `${tm.name} ${tm.surname}`,
        value: tm.id
      }))
    }
  ];

  return <Modal show={show} onClose={onClose}>
    <Form
      inputFields={inputFields}
      validationSchema={schema}
      submitLabel='Registruoti'
      onSubmit={onSubmit}
    />
  </Modal>;
};

RegisterTeamMemberFormModal.propTypes = {
  show: PropTypes.bool.isRequired,
  teamMembers: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default RegisterTeamMemberFormModal;