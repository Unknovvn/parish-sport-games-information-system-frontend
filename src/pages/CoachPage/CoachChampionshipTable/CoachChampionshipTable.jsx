import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import AuthContext from '../../../store/authContext';
import paths from '../../../constants/paths';
import { getCoachesChampopnships } from '../../../services/championship';
import { toInputDateFormat } from '../../../utils/dates';
import Table from '../../../components/Table';
import OutlineButton from '../../../components/OutlineButton';
import styles from './CoachChampionshipTable.module.scss';

const CoachChampionshipTable = () => {
  const history = useHistory();
  const { user } = useContext(AuthContext);
  const [championships, setChampionships] = useState([]);
  const [loading, setLoading] = useState(false);

  const loadCoachesChampionships = () => {
    getCoachesChampopnships(user.id)
      .then(data => {
        setChampionships(data);
        setLoading(false);
      });
  };

  const onRegisterTeam = (championshipId) => {
    const path = paths.createTeam.replace(':championshipId', championshipId);
    history.push(path);
  };

  const onEditTeam = (championshipId) => {
    const path = paths.editTeam
      .replace(':championshipId', championshipId)
      .replace(':coachId', user.id);

    history.push(path);
  };

  const navigateToCompetitionList = (championshipId) => {
    const path = paths.coachCompetitions.replace(':championshipId', championshipId);
    history.push(path);
  };

  useEffect(() => {
    loadCoachesChampionships();
  }, [user]);

  const tableColumns = [
    {
      header: 'Pavadinimas',
      value: 'name'
    },
    {
      header: 'Data',
      render: (row) => <span>{toInputDateFormat(row.date)}</span>
    },
    {
      header: '',
      value: 'organize-team',
      render: (row) => row.isRegistered
        ? !row.finished && <OutlineButton onClick={() => onEditTeam(row.id)}>Redaguoti komandą</OutlineButton>
        : <OutlineButton onClick={() => onRegisterTeam(row.id)}>Registruoti komandą</OutlineButton>
    },
    {
      header: '',
      value: 'competition-list',
      render: row => row.isRegistered && <OutlineButton onClick={() => navigateToCompetitionList(row.id)}>
        Varžybų sąrašas
      </OutlineButton>
    }
  ];

  return <>
    <div className={styles.tableContainer}>
      <Table
        columns={tableColumns}
        data={championships}
        loading={loading}
        noRecordsLabel='Nėra čempionatų, kuriuose galima dalyvauti'
      />
    </div>
  </>;
};

export default CoachChampionshipTable;