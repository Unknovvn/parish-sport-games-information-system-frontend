import React, { useContext, useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import paths from '../../../constants/paths';
import { getByChampionshipIdAndCoachId, editTeam, createTeam } from '../../../services/team';
import AuthContext from '../../../store/authContext';
import Input from '../../../components/Input';
import Button from '../../../components/Button';
import Table from '../../../components/Table';
import OutlineButton from '../../../components/OutlineButton';
import TeamMemberFormModal from '../TeamMemberFormModal';
import styles from './TeamMembersTable.module.scss';

const TeamMembersTable = () => {
  const history = useHistory();
  const { user } = useContext(AuthContext);
  const { championshipId } = useParams();
  const [team, setTeam] = useState({});
  const [loading, setLoading] = useState(false);
  const [showTeamMemberFormModal, setShowTeamMemberFormModal] = useState(false);
  const [teamMemberFormModel, setTeamMemberFormModel] = useState({});

  const loadTeam = () => {
    setLoading(true);
    getByChampionshipIdAndCoachId(championshipId, user.id)
      .then(data => {
        if (data) {
          setTeam(data);
        }

        setLoading(false);
      });
  };

  useEffect(() => {
    loadTeam();
  }, []);

  const onEditTeamMemberInit = teamMember => {
    setTeamMemberFormModel(teamMember);
    setShowTeamMemberFormModal(true);
  };

  const onEditTeamMember = (formModel) => {
    const changedTeamMembers = [...team.members];
    const changedTeamMember = changedTeamMembers.find(c => teamMemberFormModel.id
      ? c.id === teamMemberFormModel.id
      : c.position === teamMemberFormModel.position);

    if (changedTeamMember) {
      const index = changedTeamMembers.indexOf(changedTeamMember);
      changedTeamMembers[index] = formModel;
      setTeam({ ...team, members: changedTeamMembers });
    }

    setTeamMemberFormModel({});
    setShowTeamMemberFormModal(false);
  };

  const onCreateTeamMemberInit = () => {
    setShowTeamMemberFormModal(true);
  };

  const onCreateTeamMember = (formModel) => {
    setTeamMemberFormModel({});
    setShowTeamMemberFormModal(false);
    if (team.members && team.members.length > 0) {
      const teamMember = { position: team.members.length + 1, ...formModel };
      setTeam({ ...team, members: [...team.members, teamMember] });
    } else {
      const teamMember = { position: 1, ...formModel };
      setTeam({ ...team, members: [teamMember] });
    }
  };

  const onFormModalClose = () => {
    setTeamMemberFormModel({});
    setShowTeamMemberFormModal(false);
  };

  const onTeamNameChange = ({ target: input }) => {
    const changedTeam = { ...team };
    changedTeam.name = input.value;
    setTeam(changedTeam);
  };

  const onEditTeamSubmit = () => {
    editTeam(team)
      .then(data => {
        if (data) {
          history.push(paths.coachChampionships);
        }
      });
  };

  const onCreateTeamSubmit = () => {
    const model = { ...team, championshipId, coachId: user.id };
    createTeam(model)
      .then(data => {
        if (data) {
          history.push(paths.coachChampionships);
        }
      });
  };

  const tableColumns = [
    {
      header: 'Vardas',
      value: 'name'
    },
    {
      header: 'Pavardė',
      value: 'surname'
    },
    {
      header: '',
      value: 'edit',
      render: (row) => <OutlineButton onClick={() => onEditTeamMemberInit(row)}>Redaguoti</OutlineButton>
    }
  ];

  return <>
    <TeamMemberFormModal
      teamMember={teamMemberFormModel}
      show={showTeamMemberFormModal}
      onCreate={onCreateTeamMember}
      onEdit={onEditTeamMember}
      onClose={onFormModalClose}
    />
    <div className={styles.headerDiv}>
      <div className={styles.teamName}>
        <Input name='name' label='Komandos pavadinimas' value={team.name || ''} handleChange={onTeamNameChange}/>
      </div>
      <div className={styles.addTeamMemberBtn}>
        <Button onClick={onCreateTeamMemberInit}>
          Pridėti komandos narį
        </Button>
      </div>
    </div>
    <Table
      loading={loading}
      columns={tableColumns}
      data={team.members || []}
      noRecordsLabel='Komandoje nėra narių'
    />
    <div className={styles.footerDiv}>
      <div className={styles.buttons}>
        <OutlineButton onClick={() => history.push(paths.coachChampionships)}>
          Grįžti į čempionatų sąrašą
        </OutlineButton>
        {team.id
          ? <Button onClick={() => onEditTeamSubmit()}>
            Redaguoti
          </Button>
          : <Button onClick={() => onCreateTeamSubmit()}>
            Sukurti
          </Button>
        }
      </div>
    </div>
  </>;
};

export default TeamMembersTable;