import React from 'react';
import PropTypes from 'prop-types';
import schema from '../../../validations/team-member-form';
import Modal from '../../../components/Modal';
import Form from '../../../components/Form';

const TeamMemberFormModal = ({ teamMember, show, onCreate, onEdit, onClose }) => {
  const isNewTeamMember = Object.keys(teamMember).length === 0;
  const inputFields = [
    {
      name: 'name',
      label: 'Vardas',
      initialValue: teamMember.name || ''
    },
    {
      name: 'surname',
      label: 'Pavardė',
      initialValue: teamMember.surname || ''
    }
  ];

  return <Modal show={show} onClose={onClose}>
    <Form
      inputFields={inputFields}
      validationSchema={schema}
      submitLabel={isNewTeamMember ? 'Sukurti' : 'Redaguoti'}
      onSubmit={isNewTeamMember ? onCreate : onEdit}
    />
  </Modal>;
};

TeamMemberFormModal.propTypes = {
  teamMember: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  onCreate: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default TeamMemberFormModal;