import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import paths from '../../constants/paths';
import AuthContext from '../../store/authContext';
import { login } from '../../services/auth';
import schema from '../../validations/login';
import Form from '../../components/Form';
import MainPicture from '../../assets/images/main-picture.png';
import styles from './loginForm.module.scss';

const LoginForm = () => {
  const history = useHistory();
  const { onLogin } = useContext(AuthContext);

  const handleFormSubmit = async (data) => {
    const currentUser = await login(data);
    if (currentUser) {
      onLogin(currentUser);
      history.push(paths.mainPage);
    }
  };

  const inputFields = [
    {
      name: 'username',
      label: 'Prisijungimo vardas',
      initialValue: ''
    },
    {
      name: 'password',
      label: 'Slaptažodis',
      initialValue: '',
      type: 'password'
    }
  ];

  return (
    <div>
      <div>
        <img src={MainPicture} className={styles.mainPictureContainer} alt='main'/>
      </div>
      <div className={styles.formContainer}>
        <h2>Prisijungti</h2>
        <Form
          inputFields={inputFields}
          validationSchema={schema}
          submitLabel="Prisijungti"
          onSubmit={handleFormSubmit}
        />
      </div>
    </div>
  );
};

export default LoginForm;
