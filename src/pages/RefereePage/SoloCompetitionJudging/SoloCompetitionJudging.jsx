import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import paths from '../../../constants/paths';
import { getById, getSoloCompetitors, finishSoloCompetition } from '../../../services/competition';
import Table from '../../../components/Table';
import OutlineButton from '../../../components/OutlineButton';
import Input from '../../../components/Input';
import styles from './SoloCompetitionJudging.module.scss';
import Button from '../../../components/Button';

const SoloCompetitionJudging = () => {
  const history = useHistory();
  const { championshipId, competitionId } = useParams();
  const [competition, setCompetition] = useState(null);
  const [loading, setLoading] = useState(false);
  const [competitors, setCompetitors] = useState([]);
  const [results, setResults] = useState([]);

  const loadCompetition = () => {
    setLoading(true);
    getById(competitionId)
      .then(data => {
        if (data)
          setCompetition(data);
        setLoading(false);
      });
  };

  const loadCompetitors = () => {
    setLoading(true);
    getSoloCompetitors(competitionId)
      .then(data => {
        setCompetitors(data);
        setLoading(false);
      });
  };

  useEffect(() => {
    loadCompetition();
    loadCompetitors();
  }, [competitionId]);

  const navigateToCompetitionList = () => {
    const path = paths.refereeCompetitions.replace(':championshipId', championshipId);
    history.push(path);
  };

  const handleResultChange = (competitorId, { target: input }) => {
    const model = { competitorId, result: input.value };
    const changedResults = [...results];
    const changedResult = changedResults.find(c => c.competitorId === competitorId);
    if (changedResult) {
      const index = changedResults.indexOf(changedResult);
      changedResults[index] = model;
      setResults(changedResults);
    } else {
      setResults([...changedResults, model]);
    }
  };

  const getCompetitorsResult = competitorId => {
    const competitorResult = results.find(r => r.competitorId === competitorId);
    if (competitorResult) {
      return competitorResult.result;
    }

    return '';
  };

  const finishCompetition = () => {
    finishSoloCompetition(competitionId, results)
      .then(success => {
        if (success)
          navigateToCompetitionList();
      });
  };

  const tableColumns = [
    {
      header: 'Vardas',
      value: 'name'
    },
    {
      header: 'Pavardė',
      value: 'surname'
    },
    {
      header: 'Rezultatas',
      value: 'result-input',
      render: row => <div className={styles.resultInput}>
        <Input
          name='result'
          label=''
          value={getCompetitorsResult(row.id) || ''}
          handleChange={event => handleResultChange(row.id, event)}
        />
      </div>
    }
  ];

  return <>
    <div className={styles.headerDiv}>
      <h2>{competition && competition.name}</h2>
    </div>
    <div className={styles.tableContainer}>
      <Table
        columns={tableColumns}
        data={competitors}
        loading={loading}
        noRecordsLabel='Varžybos neturi registruotų dalyvių'
      />
    </div>
    <div className={styles.footerDiv}>
      <div className={styles.buttons}>
        <OutlineButton onClick={() => navigateToCompetitionList()}>
          Grįžti į varžybų sąrašą
        </OutlineButton>
        <Button onClick={() => finishCompetition()}>
          Baigti varžybas
        </Button>
      </div>
    </div>
  </>;
};

export default SoloCompetitionJudging;
