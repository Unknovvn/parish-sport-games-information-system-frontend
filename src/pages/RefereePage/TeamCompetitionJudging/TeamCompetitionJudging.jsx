import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import paths from '../../../constants/paths';
import { finishTeamCompetition, getTeamCompetitors } from '../../../services/competition';
import Table from '../../../components/Table';
import OutlineButton from '../../../components/OutlineButton';
import styles from './TeamCompetitionJudging.module.scss';
import Button from '../../../components/Button';

const TeamCompetitionJudging = () => {
  const history = useHistory();
  const { championshipId, competitionId } = useParams();
  const [loading, setLoading] = useState(true);
  const [competitors, setCompetitors] = useState([]);

  const loadCompetitors = () => {
    getTeamCompetitors(competitionId)
      .then(data => {
        setCompetitors(data);
        setLoading(false);
      });
  };

  useEffect(() => {
    loadCompetitors();
  }, [competitionId]);

  const navigateToCompetitionList = () => {
    const path = paths.refereeCompetitions.replace(':championshipId', championshipId);
    history.push(path);
  };

  const changePlaceUp = competitor => {
    const changedCompetitors = [...competitors];
    const index = changedCompetitors.indexOf(competitor);
    const temp = changedCompetitors[index - 1];
    changedCompetitors[index - 1] = competitor;
    changedCompetitors[index] = temp;
    setCompetitors(changedCompetitors);
  };

  const changePlaceDown = competitor => {
    const changedCompetitors = [...competitors];
    const index = changedCompetitors.indexOf(competitor);
    const temp = changedCompetitors[index + 1];
    changedCompetitors[index + 1] = competitor;
    changedCompetitors[index] = temp;
    setCompetitors(changedCompetitors);
  };

  const finishCompetition = () => {
    const model = competitors.map(c => ({
      competitorId: c.id,
      place: competitors.indexOf(c) + 1
    }));
    finishTeamCompetition(competitionId, model)
      .then(success => {
        if (success)
          navigateToCompetitionList();
      });
  };

  const tableColumns = [
    {
      header: 'Vieta',
      value: 'place',
      render: row => competitors.indexOf(row) + 1
    },
    {
      header: 'Pavadinimas',
      value: 'name'
    },
    {
      header: '',
      value: 'change-place',
      render: row => <div className={styles.buttons}>
        {competitors.indexOf(row) !== 0 && <OutlineButton onClick={() => changePlaceUp(row)}>
          <span className='fa fa-caret-up'/>
        </OutlineButton>
        }
        {competitors.indexOf(row) !== competitors.length - 1 && <OutlineButton onClick={() => changePlaceDown(row)}>
          <span className='fa fa-caret-down'/>
        </OutlineButton>}
      </div>
    }
  ];

  return <>
    <div className={styles.tableContainer}>
      <Table
        columns={tableColumns}
        data={competitors}
        loading={loading}
        noRecordsLabel='Varžybos neturi registruotų dalyvių'
      />
    </div>
    <div className={styles.footerDiv}>
      <div className={styles.buttons}>
        <OutlineButton onClick={() => navigateToCompetitionList()}>
          Grįžti į varžybų sąrašą
        </OutlineButton>
        <Button onClick={() => finishCompetition()}>
          Baigti varžybas
        </Button>
      </div>
    </div>
  </>;
};

export default TeamCompetitionJudging;