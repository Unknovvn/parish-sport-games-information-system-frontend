import React from 'react';
import { Redirect, Switch } from 'react-router-dom';
import paths from '../../constants/paths';
import Navbar from '../../components/Navbar';
import ProtectedRoute from '../../components/ProtectedRoute';
import ChangePassword from '../HomePage/ChangePassword';
import Container from '../../components/Container';
import RefereeChampionshipTable from './RefereeChampionshipTable';
import RefereeCompetitionTable from './RefereeCompetitionTable';
import SoloCompetitionJudging from './SoloCompetitionJudging';
import TeamCompetitionJudging from './TeamCompetitionJudging';

const RefereePage = () => {
  const navbarLinks = [
    {
      label: 'Čempionatų lentelė',
      path: paths.refereeChampionships
    }
  ];

  return <>
    <Navbar links={navbarLinks}/>
    <Container>
      <Switch>
        <ProtectedRoute exact path={paths.changePassword} component={ChangePassword}/>
        <ProtectedRoute exact path={paths.refereeChampionships} component={RefereeChampionshipTable}/>
        <ProtectedRoute exact path={paths.refereeCompetitions} component={RefereeCompetitionTable}/>
        <ProtectedRoute exact path={paths.soloCompetitionJudging} component={SoloCompetitionJudging}/>
        <ProtectedRoute exact path={paths.teamCompetitionJudging} component={TeamCompetitionJudging}/>
        <Redirect to={paths.refereeChampionships}/>
      </Switch>
    </Container>
  </>;
};

export default RefereePage;
