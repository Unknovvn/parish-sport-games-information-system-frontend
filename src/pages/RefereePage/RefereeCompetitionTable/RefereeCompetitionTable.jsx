import React, { useContext, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import paths from '../../../constants/paths';
import AuthContext from '../../../store/authContext';
import { getRefereesCompetitions, activateCompetition } from '../../../services/competition';
import { isTeamCompetition } from '../../../utils/resultType';
import Table from '../../../components/Table';
import OutlineButton from '../../../components/OutlineButton';
import DialogModal from '../../../components/DialogModal/DialogModal';
import styles from './RefereeCompetitionTable.module.scss';

const RefereeCompetitionTable = () => {
  const history = useHistory();
  const { championshipId } = useParams();
  const { user } = useContext(AuthContext);
  const [loading, setLoading] = useState(true);
  const [competitions, setCompetitions] = useState([]);
  const [showActivateCompetitionModal, setShowActivateCompetitionModal] = useState(false);
  const [competitionToStart, setCompetitionToStart] = useState(null);

  const loadRefereeCompetitions = () => {
    getRefereesCompetitions(championshipId, user.id)
      .then(data => {
        setCompetitions(data);
        setLoading(false);
      });
  };

  useEffect(() => {
    loadRefereeCompetitions();
  }, [championshipId, user.id]);

  const startCompetitionInit = competition => {
    setCompetitionToStart(competition);
    setShowActivateCompetitionModal(true);
  };

  const startCompetition = () => {
    activateCompetition(competitionToStart.id)
      .then(data => {
        if (data) {
          navigateToJudging(competitionToStart);
        }
      });
  };

  const navigateToJudging = competition => {
    const parameterlessPath = isTeamCompetition(competition.resultTypeId)
      ? paths.teamCompetitionJudging
      : paths.soloCompetitionJudging;

    const path = parameterlessPath
      .replace(':championshipId', championshipId)
      .replace(':competitionId', competition.id);
    history.push(path);
  };

  const closeStartCompetitionModal = () => {
    setCompetitionToStart(null);
    setShowActivateCompetitionModal(false);
  };

  const tableColumns = [
    {
      header: 'Pavadinimas',
      value: 'name'
    },
    {
      header: '',
      value: 'begin-competition',
      render: row => <OutlineButton
        onClick={() => row.active ? navigateToJudging(row) : startCompetitionInit(row)}>
        {row.active ? 'Tęsti teisėjavimą' : 'Pradėti varžybas'}
      </OutlineButton>
    }
  ];

  return <>
    <DialogModal
      title='Ar tikrai norite pradėti varžybas?'
      show={showActivateCompetitionModal}
      onAgree={startCompetition}
      onClose={() => closeStartCompetitionModal()}
    />
    <div className={styles.tableContainer}>
      <Table
        columns={tableColumns}
        data={competitions}
        loading={loading}
        noRecordsLabel='Čempionatas neturi jums teisėjauti skirtų varžybų'
      />
    </div>
    <div className={styles.footerDiv}>
      <OutlineButton onClick={() => history.push(paths.refereeChampionships)}>
        Grįžti į čempionatų sąrašą
      </OutlineButton>
    </div>
  </>;
};

export default RefereeCompetitionTable;
