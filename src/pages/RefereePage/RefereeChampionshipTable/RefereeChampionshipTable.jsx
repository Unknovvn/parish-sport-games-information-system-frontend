import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import paths from '../../../constants/paths';
import { toInputDateFormat } from '../../../utils/dates';
import { getActiveChampionships } from '../../../services/championship';
import Table from '../../../components/Table';
import OutlineButton from '../../../components/OutlineButton';
import styles from './RefereeChampionshipTable.module.scss';

const RefereeChampionshipTable = () => {
  const history = useHistory();
  const [championships, setChampionships] = useState([]);
  const [loading, setLoading] = useState(true);

  const loadChampionships = () => {
    getActiveChampionships()
      .then(data => {
        setChampionships(data);
        setLoading(false);
      });
  };

  useEffect(() => {
    loadChampionships();
  }, []);

  const navigateToCompetitionList = championshipId => {
    const path = paths.refereeCompetitions.replace(':championshipId', championshipId);
    history.push(path);
  };

  const tableColumns = [
    {
      header: 'Pavadinimas',
      value: 'name'
    },
    {
      header: 'Data',
      render: row => <span>{toInputDateFormat(row.date)}</span>
    },
    {
      header: '',
      value: 'competition-list',
      render: row => <OutlineButton onClick={() => navigateToCompetitionList(row.id)}>
        Varžybų sąrašas
      </OutlineButton>
    }
  ];

  return <>
    <div className={styles.tableContainer}>
      <Table
        columns={tableColumns}
        data={championships}
        loading={loading}
        noRecordsLabel='Nėra aktyvių čempionatų'
      />
    </div>
  </>;
};

export default RefereeChampionshipTable;