import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import paths from '../../constants/paths';
import { getFinishedChampionships } from '../../services/championship';
import { toInputDateFormat } from '../../utils/dates';
import OutlineButton from '../../components/OutlineButton';
import styles from '../BoardPage/BoardChampionshipTable/BoardChampionshipTable.module.scss';
import Table from '../../components/Table';

const HistoryChampionshipsTable = () => {
  const history = useHistory();
  const [championships, setChampionships] = useState([]);
  const [loading, setLoading] = useState(false);

  const loadChampionships = () => {
    getFinishedChampionships()
      .then(data => {
        setChampionships(data);
        setLoading(false);
      });
  };

  const navigateToResults = championshipId => {
    const path = paths.historyResults.replace(':championshipId', championshipId);
    history.push(path);
  };

  useEffect(() => {
    loadChampionships();
  }, []);

  const tableColumns = [
    {
      header: 'Pavadinimas',
      value: 'name'
    },
    {
      header: 'Data',
      render: row => <span>{toInputDateFormat(row.date)}</span>
    },
    {
      header: '',
      value: 'watch-results',
      render: row => <OutlineButton onClick={() => navigateToResults(row.id)}>
        Peržiūrėti rezultatus
      </OutlineButton>
    }
  ];

  return <>
    <div className={styles.tableContainer}>
      <Table
        columns={tableColumns}
        data={championships}
        loading={loading}
        noRecordsLabel='Nėra užbaigtų čempionatų'
      />
    </div>
  </>;
};

export default HistoryChampionshipsTable;