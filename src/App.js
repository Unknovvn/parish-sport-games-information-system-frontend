import React, { useState } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import AuthContext from './store/authContext';
import LoginForm from './pages/LoginForm';
import { toast, ToastContainer } from 'react-toastify';
import paths from "./constants/paths";
import { getCurrentUser } from "./services/auth";
import ProtectedRoute from './components/ProtectedRoute';
import HomePage from './pages/HomePage';
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
    const [user, setUser] = useState(getCurrentUser());
    const handleLogin = (user) => {
      setUser(user);
    };

    const handleLogout = () => {
      setUser(null);
    };

    return (
      <>
        <ToastContainer position={toast.POSITION.TOP_CENTER} autoClose={4000}/>
        <BrowserRouter>
          <AuthContext.Provider value={{user, onLogin: handleLogin, onLogout: handleLogout}}>
            <Switch>
              <Route exact path={paths.loginPage} component={LoginForm}/>
              <ProtectedRoute path={paths.mainPage} component={HomePage}/>
              <Redirect to={paths.mainPage}/>
            </Switch>
          </AuthContext.Provider>
        </BrowserRouter>
      </>
    );
  }
;

export default App;
